CONTAINER_NAME ?= lime-sna
CONTAINER_REPO ?= registry.gitlab.com/pantacor/c-green1/limesna
CONTAINER_VERSION ?= ARM32V7
EXPORT_PATH := $(shell mktemp -t $(CONTAINER_NAME).tgz.XXXXXX)
EXPORT_SQUASH_PATH := $(shell mktemp -t $(CONTAINER_NAME).XXXXXX).squashfs
EXPORT_RAW_DIR := $(shell mktemp -d -t $(CONTAINER_NAME).raw.XXXXXX)

ifeq ($(DEBUG),)
QUIET = @
endif

build:
	mkdir tmp; wget -O tmp/qemu-arm-static https://github.com/multiarch/qemu-user-static/releases/download/v2.12.0-1/qemu-arm-static; chmod a+x tmp/qemu-arm-static
	docker build -f Dockerfile.$(CONTAINER_VERSION) --no-cache -t $(CONTAINER_REPO):$(CONTAINER_VERSION) .

clean:
	rm -fr tmp/
